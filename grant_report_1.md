
# {Project LaunchPad} - Grant Report #{1}


![](NmhhMS5wbmc.webp)

## Project Update
In our grant proposal, we had proposed creating an educational resources to teach the masses about web-monetization and the various catches involved while deciding on the platform and monetization methods over the web. Hence, coming up with a gameplay that’s educational, fun and easy to grasp, all at the same time, was the hardest milestone for us at Project LaunchPad and we are successfully past it after many rounds of play testings, feedback and improvements. 

What comes next for us is to build the required assets and infrastructure to enable users to be able to access and play them easily, without making it very `technology/platform/operating system` dependent. 

To ensure that we are able to meet our objective in the given timeframe, we kicked off some efforts in a parallel.  The details of our progress are in the report  below.

## Progress on objectives
The initial objective of the project was to create an easy to reproduce game on web-monetization that educates players around the basics of web-monetization in the context of practical examples of monetizing their content over the web. 

We are spot-on with the progress on our primary objective, however, we took an additional step to ensure the game can be reproduced in all 3 formats - print, digital and well as an async version to ensure accessibility and inclusivity for any player who couldn’t afford a decent internet or access to a specific medium.


## Key activities

1. **Created a digital prototype to play-test and refine gameplay**
Being remotely distributed, we worked on a digital prototype for the game concept using Tabletop Simulator, to play-test and refine the gameplay.
2. **Started the illustration work for the work**
We commissioned a digital artist with an exceptional portfolio to illustrate the visual assets for the game we’re creating as a part of the project.
3. **Wrote a blog on web-monetization**
We wrote a blog on prototype to educate readers about Web monetization in the simplest of language
4. **Presented at CC Summit** - We presented about the work we are doing as part of project LaunchPad at the recently concluded Creative Commons Global Summit 2021.
5. **Started work and made big progress on game web-app backend work**: We started work on the game-app the game and made an immense progress on the back-end work. Can be seen on [our GitLab repository](https://gitlab.com/projectlaunchpad/mobileapp).


## Communications and marketing
*We have set up a [twitter page](https://twitter.com/hilaunchpad) for communications and marketing.* The page doesn’t see a lot of activity today since we are focusing our energy on building the game currently before we talk about it widely.

**CC Global Summit 2021 -** We presented about project LaunchPad in the recently concluded Creatives Commons Global Summit 2021. This was a very fitting platform to speak about the project for the first time, because we learnt about Grant for the Web for the first time during a Creative Commons event back in 2019. There was a lot of interest shown in the presentation and it’s content. We also anticipated 

## What’s next?
### We are yet to make a decent progress in the following areas:
1. UX refinement and frontend work for web-app
2. Completing illustrations for the game, logo and box-art
3. Graphic design work for the gameplay booklet
4. Translating the gameplay to a messenger friendly version using the same assets

## What community support would benefit your project?
We would love some support from the community to play-test the different versions of the game and to spread the word about it in months to come. We would ensure we develop communication kits that are easy to circulate and consume. 

There would be need to test the gameplay with audience of different age-groups and demographies. For this we would especially need help from the educators in the community to learn how to approach these audiences and the points that we should keep in mind while engaging with them.

We are trying to keep the game very minimally language dependent. However, going forward, when the games evolves to a satisfactory level, we would also need help with translating the game-play rules to different languages.


## Additional comments
We are using our connections in the gaming industry to learn best practices, distribution techniques and at the same time the future prospects for the game that is following all the open standards, which is not quite a norm for the industry yet. 

Besides the progress on the game, we also want to document our experience working on a game that takes into account the limitations to accessing a content and also consuming it.

We are always open to discussions and suggestions. Please reach out to use(Michael and Veethika) to discuss any scope for collaboration. 


## Relevant links/resources  (optional)
Our Gitlab Group of projects: https://gitlab.com/projectlaunchpad
Our twitter page: https://twitter.com/hilaunchpad 
