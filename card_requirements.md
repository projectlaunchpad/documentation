We need the following cards with elements on them as card frames:

# Cards

Elements should always be placed on definable areas of a card, that means borders or exactly in the center of the card. Arbitrary placements makes implementing the app more difficult.

## Resource Cards
- Name
- Artwork
- Color (Color and Icon)
- Special Effect (Icon)
- Cost (1-3, probably Icons)

## Project Cards
- Name
- Artwork
- Colors (1-3, Color and Icon)
- Sell Price (Table with numbers, probably Icons)

We need the project cards horizontal *and* vertical

## Event Cards
- Name
- Artwork
- Special Effect (Icon)

## Persona Card
- Name
- Artwork
- Special Effect (Icon)

